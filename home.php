<?php
	session_start();
 	if (isset($_SESSION['seid']) && $_SESSION['seid'] =="wataaah"){
 		
 	} else {
 		header("Location: /index.php");
 	}
?>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>SIMPLE MANAGER</title>
		<link rel="stylesheet" type="text/css" href="/css/reset.css" />
		<link rel="stylesheet" type="text/css" href="css/styles.css" />
		<link rel="stylesheet" type="text/css" href="css/jquery.mobile-1.3.0.min.css" />
		
	</head>
	
	<body>
				
		<!-- HOME -->
		<div data-role="page" id="home">
			<!-- PANEL -->
			<div data-role="panel" id="profile" data-position="right" data-display="overlay" class="ui-reponsive" >
				<ul data-role="listview" id="profileList">
					<li data-role="list-divider">MENU</li>
					<li><a onclick="logout()">LOGOUT</a></li>
					<li><a onclick="loadProfile(); this.onclick=null;">PERFIL</a></li>
				</ul>
			</div>	
			<!-- HEADER -->	
			<div data-role="header" data-position="fixed" class="ui-icon-nodisc" data-id="TopBar" data-theme="b">
				<a href="#profile" class="ui-btn-right" data-dismisable="true">PROFILE</a>
				<h1>HOME</h1>
			</div>
			<!-- CONTENT -->
			<div id="contenido" data-role="content">

					<div data-role="controlgroup" data-type="horizontal">
					    <a href="#home" data-role="button" data-icon="home" data-iconpos="bottom">&nbsp;HOME&nbsp;</a>
					    <a href="#equipos" data-role="button" data-icon="gear" data-iconpos="bottom">EQUIPOS</a>
					    <a href="#incidencias" data-role="button" data-icon="grid" data-iconpos="bottom">INCIDENCIAS</a>
					</div>
					&nbsp;
				    <div data-role="controlgroup" data-type="horizontal">
				    	<a href="#ayuda" data-role="button" data-icon="gear" data-iconpos="bottom">AYUDA</a>
				    	<a href="#contact" data-role="button" data-icon="info" data-iconpos="bottom">CONTACTO</a>
				    	<a href="#asistencia" data-role="button" data-icon="alert" data-iconpos="bottom">ASISTENCIA</a>
					</div>
				
			</div>
			<!-- FOOTER -->
			<div data-role="footer" data-position="fixed" data-tap-toggle="false" data-id="navigationBar" data-theme="b">		
						<div data-role="navbar" id="navar" data-tap-toggle="false">
							<a href="#home">HOME</a>
							<a href="#popupMenu" data-rel="popup" data-transition="flip">INCIDENCES</a>
									<div data-role="popup" id="popupMenu">
										<a href="#incidencias" icon="">VER</a>
									    <a href="#newincidencia">NUEVA</a>
									</div>
							<a href="#equipos">EQUIPOS</a>
							<a href="#ayuda">AYUDA</a>
							<a href="#contact">CONTACTO</a>
							<a href="#asistencia">ASISTENCIA</a>								
						</div> <!-- NAVBAR-->
			</div><!-- FOOTER -->
		</div>
		
		<!-- INCIDENCIAS -->
		<div data-role="page" id="incidencias">
			<!-- PANEL -->
			<div data-role="panel" id="profile" data-position="right" data-display="overlay" class="ui-reponsive" >
				<ul data-role="listview">
					<li data-role="list-divider">MENU</li>
					<li><a onclick="logout()">LOGOUT</a></li>
					<li><a onclick="loadProfile(); this.onclick=null;">PERFIL</a></li>
				</ul>
			</div>	
			<!-- HEADER -->	
			<div data-role="header" data-position="fixed" class="ui-icon-nodisc" data-id="TopBar" data-theme="b">
				<a href="#profile" class="ui-btn-right" data-dismisable="true">PROFILE</a>
				<h1>INCIDENCIAS</h1>
			</div>
			<!-- CONTENT -->
			<div id="contenid" data-role="content">
						<div id="legend">
							<ul>
								<li class="legend-id">ID</li>
								<li class="legend-title">TITLE</li>
								<li class="legend-fecha">FECHA</li>
								<li class="legend-gestor">GESTOR</li>
								<li class="legend-estado">ESTADO</li>
							</ul>
						</div>
						<ul data-role="listview" data-filter="true" id="table-body"></ul>
					
			</div>
			<!-- FOOTER -->
			<div data-role="footer" data-position="fixed" data-tap-toggle="false" data-id="navigationBar" data-theme="b">		
						<div data-role="navbar" id="navar" data-tap-toggle="false">
							<a href="#home">HOME</a>
							<a href="#popupMenu" data-rel="popup" data-transition="flip">INCIDENCES</a>
									<div data-role="popup" id="popupMenu">
										<a href="#incidencias" icon="">VER</a>
									    <a href="#newincidencia">NUEVA</a>
									</div>
							<a href="#equipos">EQUIPOS</a>
							<a href="#ayuda">AYUDA</a>
							<a href="#contact">CONTACTO</a>
							<a href="#asistencia">ASISTENCIA</a>								
						</div> <!-- NAVBAR-->
			</div><!-- FOOTER -->
		</div>
		
		<!-- NEW INCIDENCE -->
		<div data-role="page" id="newincidencia">
			<!-- PANEL -->
			<div data-role="panel" id="profile" data-position="right" data-display="overlay" class="ui-reponsive">
				<ul data-role="listview">
					<li data-role="list-divider">MENU</li>
					<li><a onclick="logout()">LOGOUT</a></li>
					<li><a onclick="loadProfile(); this.onclick=null;">PERFIL</a></li>
				</ul>
			</div>	
			<!-- HEADER -->	
			<div data-role="header" data-position="fixed" class="ui-icon-nodisc" data-id="TopBar" data-theme="b">
				<a href="#profile" class="ui-btn-right" data-dismisable="true">PROFILE</a>
				<h1>NUEVA INCIDENCIA</h1>
			</div>
			<!-- CONTENT -->
			<div id="contenido" data-role="content">
			<div data-role="fieldcontain">
			    <label for="TitleIncidencia">Titulo Incidencia</label>
			    <input type="text" name="TitleIncidencia" id="TitleIncidencia" placeholder="Inserte el titulo de la incidencia" value="">
			</div>
			<div data-role="fieldcontain">
			    <label for="DescripcionIncidencia">Descripción Incidencia</label>
			    <textarea data-mini="true" cols="40" rows="8" name="DescripcionIncidencia" id="DescripcionIncidencia" placeholder="Detalle la incidencia"></textarea>
			</div>
			<div data-role="fieldcontain">
			    <label for="Date" >Fecha de hoy</label>
			    <input type="text" name="DAte" id="Date">
			</div>
			

		<a id="SaveIncidence" data-role="button" onclick="SaveIncidencia()">ABRIR INCIDENCIA</a>

				
			</div>
			<!-- FOOTER -->
			<div data-role="footer" data-position="fixed" data-tap-toggle="false" data-id="navigationBar" data-theme="b">		
						<div data-role="navbar" id="navar" data-tap-toggle="false">
							<a href="#home">HOME</a>
							<a href="#popupMenu" data-rel="popup" data-transition="flip">INCIDENCES</a>
									<div data-role="popup" id="popupMenu">
										<a href="#incidencias" icon="">VER</a>
									    <a href="#newincidencia">NUEVA</a>
									</div>
							<a href="#equipos">EQUIPOS</a>
							<a href="#ayuda">AYUDA</a>
							<a href="#contact">CONTACTO</a>
							<a href="#asistencia">ASISTENCIA</a>								
						</div> <!-- NAVBAR-->
			</div><!-- FOOTER -->
			
		</div>
		
		<!-- DETAILED INCIDENCE -->
		<div data-role="page" id="IncidenceDetail">
			<!-- PANEL -->
			<div data-role="panel" id="profile" data-position="right" data-display="overlay" class="ui-reponsive">
				<ul data-role="listview">
					<li data-role="list-divider">MENU</li>
					<li><a onclick="logout()">LOGOUT</a></li>
					<li><a onclick="loadProfile(); this.onclick=null;">PERFIL</a></li>
				</ul>
			</div>	
			<!-- HEADER -->	
			<div data-role="header" data-position="fixed" class="ui-icon-nodisc" data-id="TopBar" data-theme="b">
				<a href="#profile" class="ui-btn-right" data-dismisable="true">PROFILE</a>
				<h1>INCIDENCIAS</h1>
			</div>
			<!-- CONTENT -->
			<div id="contenido" data-role="content">
						<div data-role="fieldcontain">
						    <label for="TIncidencia">Titulo Incidencia</label>
						    <input type="text" name="TIncidencia" id="TIncidencia" placeholder="Inserte el titulo de la incidencia" value="">
						</div>
						<div data-role="fieldcontain">
						    <label for="DIncidencia">Descripción Incidencia</label>
						    <textarea data-mini="true" cols="40" rows="8" name="DIncidencia" id="DIncidencia" placeholder="Detalle la incidencia"></textarea>
						</div>
						<div data-role="fieldcontain">
						    <label for="TDate" >Fecha de hoy</label>
						    <input type="text" name="TDAte" id="TDate" value="" disable="true">
						</div>
						<select id="StateIncidencia">
						  	<option value="1">ABIERTA</option>
						  	<option value="2">CERRADA</option>
						</select>
						<a id="Id"  style="visibility: hidden"></a>
						<a id="SaveIncidence" data-role="button" onclick="UpdateIncidencia()">MODIFICAR INCIDENCIA</a>
						<a id="DelIncidence" data-role="button" onclick="DelIncidencia()">ELIMINAR INCIDENCIA</a>
					
						<!-- COMENTARIOS -->
							<div id="coments">
	

							</div>
						<!-- COMENTARIO NUEVO MALDITO BASTARDO -->
						<br><div id="newComentario">
							<div data-role="fieldcontain">
						    	<label for="NComment">Nuevo Comentario </label>
						    		<textarea data-mini="false" cols="50" rows="8" name="NComment" id="NComment" ></textarea>
							</div>
							<a id="InComment" data-role="button" onclick="InsertComment()">Añadir nuevo comentario</a>
						</div>
			</div>
			<!-- FOOTER -->
			<div data-role="footer" data-position="fixed" data-tap-toggle="false" data-id="navigationBar" data-theme="b">		
						<div data-role="navbar" id="navar" data-tap-toggle="false">
							<a href="#home">HOME</a>
							<a href="#popupMenu" data-rel="popup" data-transition="flip">INCIDENCES</a>
									<div data-role="popup" id="popupMenu">
										<a href="#incidencias" icon="">VER</a>
									    <a href="#newincidencia">NUEVA</a>
									</div>
							<a href="#equipos">EQUIPOS</a>
							<a href="#ayuda">AYUDA</a>
							<a href="#contact">CONTACTO</a>
							<a href="#asistencia">ASISTENCIA</a>								
						</div> <!-- NAVBAR-->
			</div><!-- FOOTER -->
		</div>
		
		<!-- EQUIPOS -->
		<div data-role="page" id="equipos">
			<!-- PANEL -->
			<div data-role="panel" id="profile" data-position="right" data-display="overlay" class="ui-reponsive">
				<ul data-role="listview">
					<li data-role="list-divider">MENU</li>
					<li><a onclick="logout()">LOGOUT</a></li>
					<li><a onclick="loadProfile(); this.onclick=null;">PERFIL</a></li>
				</ul>
			</div>	
			<!-- HEADER -->	
			<div data-role="header" data-position="fixed" class="ui-icon-nodisc" data-id="TopBar" data-theme="b">
				<a href="#profile" class="ui-btn-right" data-dismisable="true">PROFILE</a>
				<h1>EQUIPOS</h1>
			</div>
			<!-- CONTENT -->
			<div id="contenido" data-role="content">
				
							<div data-role="fieldcontain">
							    <label for="NEquipo">Nombre del equipo</label>
							    <input type="text" name="NEquipo" id="NEquipo" placeholder="Inserte nombre del equipo" >
							</div>
							
							<div data-role="fieldcontain">
							    <label for="Marca">Marca del equipo</label>
							    <input type="text"  name="Marca" id="Marca" placeholder="Marca del equipo">
							</div>
							
							<div data-role="fieldcontain">
							    <label for="CodigoS" >Numero de Serie</label>
							    <input type="text" name="CodigoS" id="CodigoS"  placeholder="Inserte aqui el numero de serie de su equipo">
							</div>
							
							<div data-role="fieldcontain">
							    <label for="DateCompra" >Fecha de Compra</label>
							    <input type="text" name="DateCompra" id="DateCompra"  placeholder="Inserte la fecha de Compra">
							</div>
							
							<a id="SaveEquipo" data-role="button" onclick="UpdateEquipo()">MODIFICAR DATOS DEL EQUIPO</a>
			</div>
			<!-- FOOTER -->
			<div data-role="footer" data-position="fixed" data-tap-toggle="false" data-id="navigationBar" data-theme="b">		
						<div data-role="navbar" id="navar" data-tap-toggle="false">
							<a href="#home">HOME</a>
							<a href="#popupMenu" data-rel="popup" data-transition="flip">INCIDENCES</a>
									<div data-role="popup" id="popupMenu">
										<a href="#incidencias" icon="">VER</a>
									    <a href="#newincidencia">NUEVA</a>
									</div>
							<a href="#equipos">EQUIPOS</a>
							<a href="#ayuda">AYUDA</a>
							<a href="#contact">CONTACTO</a>
							<a href="#asistencia">ASISTENCIA</a>								
						</div> <!-- NAVBAR-->
			</div><!-- FOOTER -->	
		
		</div>
		
		<!-- ASISTENCIA -->
		<div data-role="page" id="asistencia">
		<!-- PANEL -->
			<div data-role="panel" id="profile" data-position="right" data-display="overlay" class="ui-reponsive">
				<ul data-role="listview">
					<li data-role="list-divider">MENU</li>
					<li><a onclick="logout()">LOGOUT</a></li>
					<li><a onclick="loadProfile(); this.onclick=null;">PERFIL</a></li>
				</ul>
			</div>	
			<!-- HEADER -->	
			<div data-role="header" data-position="fixed" class="ui-icon-nodisc" data-id="TopBar" data-theme="b">
				<a href="#profile" class="ui-btn-right" data-dismisable="true">PROFILE</a>
				<h1>ASISTENCIA</h1>
			</div>
			<!-- CONTENT -->
			<div id="contenido" data-role="content">
				Para ser atendido es necesario descargar el siguiente   programa<br><br>
				<a data-role="button" id="GetSoftware" href="http://download.teamviewer.com/download/TeamViewer_Setup_es.exe">DESCARGAR SOFTWARE</a>
				<br>Una vez instaldo tendremos que dar en caso de averia al tecnico <br>
				la información de conexion. Esta informacion se puede ver cuando <br>
				ejecutamos el programa y la informacion es:
				<br>
				<br> El ID -- de color Rojo en la imagen
				<br> La contraseña -- de color azul en la imagen
				<br><br><img src="css/images/TV.png" height="300px"/>
			</div>
			<!-- FOOTER -->
			<div data-role="footer" data-position="fixed" data-tap-toggle="false" data-id="navigationBar" data-theme="b">		
						<div data-role="navbar" id="navar" data-tap-toggle="false">
							<a href="#home">HOME</a>
							<a href="#popupMenu" data-rel="popup" data-transition="flip">INCIDENCES</a>
									<div data-role="popup" id="popupMenu">
										<a href="#incidencias" icon="">VER</a>
									    <a href="#newincidencia">NUEVA</a>
									</div>
							<a href="#equipos">EQUIPOS</a>
							<a href="#ayuda">AYUDA</a>
							<a href="#contact">CONTACTO</a>
							<a href="#asistencia">ASISTENCIA</a>								
						</div> <!-- NAVBAR-->
			</div><!-- FOOTER -->			
			
		</div>
		
		<!-- AYUDA -->
		<div data-role="page" id="ayuda">
		<!-- PANEL -->
			<div data-role="panel" id="profile" data-position="right" data-display="overlay" class="ui-reponsive">
				<ul data-role="listview">
					<li data-role="list-divider">MENU</li>
					<li><a onclick="logout()">LOGOUT</a></li>
					<li><a onclick="loadProfile(); this.onclick=null;">PERFIL</a></li>
				</ul>
			</div>	
			<!-- HEADER -->	
			<div data-role="header" data-position="fixed" class="ui-icon-nodisc" data-id="TopBar" data-theme="b">
				<a href="#profile" class="ui-btn-right" data-dismisable="true">PROFILE</a>
				<h1>AYUDA</h1>
			</div>
			<!-- CONTENT -->
			<div id="contenido" data-role="content">
				
				<img src="http://webiza.orgfree.com/ayuda.gif" height="100px"/></br></br>
				Como hacer un nuevo usuario y Registrarse</br></br>
				<iframe width="560" height="315" src="http://www.youtube.com/embed/pk7NLnDifFM" frameborder="0" allowfullscreen></iframe>
				</br>
				</br>
				Salir de la aplicacion
				</br>
				</br>
				<iframe width="560" height="315" src="http://www.youtube.com/embed/M55CzJKL1kI" frameborder="0" allowfullscreen></iframe>
				</br>
				</br>
				Crear una nueva Incidencia y ver la Lista de tus incidencias
				</br>
				</br>
				<iframe width="560" height="315" src="http://www.youtube.com/embed/8wyWoOQf97A" frameborder="0" allowfullscreen></iframe>
				<br>
				<br>
				Modificaciones de equipos
				<iframe width="560" height="315" src="http://www.youtube.com/embed/p4EEVcFqFbw" frameborder="0" allowfullscreen></iframe>
				<br>
				<br>
				Comentarios en incidencias
				<br>
				<br>
				<iframe width="560" height="315" src="http://www.youtube.com/embed/uCpS5JmaYoU" frameborder="0" allowfullscreen></iframe>
				<br>
				<br>
				Perfil y cambios de contraseñas
				<br>
				<br>
				<iframe width="560" height="315" src="http://www.youtube.com/embed/eV-f0QchTGg" frameborder="0" allowfullscreen></iframe>
			</div>
			<!-- FOOTER -->
			<div data-role="footer" data-position="fixed" data-tap-toggle="false" data-id="navigationBar" data-theme="b">		
						<div data-role="navbar" id="navar" data-tap-toggle="false">
							<a href="#home">HOME</a>
							<a href="#popupMenu" data-rel="popup" data-transition="flip">INCIDENCES</a>
									<div data-role="popup" id="popupMenu">
										<a href="#incidencias" icon="">VER</a>
									    <a href="#newincidencia">NUEVA</a>
									</div>
							<a href="#equipos">EQUIPOS</a>
							<a href="#ayuda">AYUDA</a>
							<a href="#contact">CONTACTO</a>
							<a href="#asistencia">ASISTENCIA</a>								
						</div> <!-- NAVBAR-->
			</div><!-- FOOTER -->	
		
		</div>
		
		<!-- CONTACTO -->
		<div data-role="page" id="contact">
			
			<div data-role="header" data-position="fixed" class="ui-icon-nodisc" data-id="TopBar" data-theme="b">
				<h3>CONTACT</h3>
				<a href="#home" class="ui-btn-right">HOME</a>
			</div>
			
			<div id="ContactData" data-role="content">
				<h1>SIMPLE MANAGER</h1>
				<img src="css/images/logo.png" width="27%"/>
					<p>EMAIl: info@simplemanager.com</p>
					<p>TELEFONO : +34 97658742</p>	
					<p>DIRECCION: Avenida del Prado Nº 1</p>		

		</div>
	</body>
	
	<!-- JS -->
	<script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="/js/jquery.mobile-1.3.0.min.js"></script>
	<script type="text/javascript" src="/js/application.js"></script>
	<script type="text/javascript">
		var user = "<?php echo $_SESSION['user']; ?>";
	</script>
</html>