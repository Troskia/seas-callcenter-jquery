/*.LIVE WEB PAGES*/
incidences = [];
IncidenceDetailedID = [];
divCloned = [];

$("#incidencias").live("pagecreate", function(){	   
	$.ajax({
		type: 'GET',
		url: 'webservice/getIncidencias.php',
		dataType: 'json',
		success: apend,
		error: function(error) {alert('ERROR:  ' + error.message)}
	});
	function apend(data){
		incidencias = data
		$.each(incidencias, function(index, incidence) {
		$('#table-body').append('<li><a onClick="GotoDetail(' + incidence.id + ')"><span class="legend-id">' + incidence.id  +'</span><span class="legend-title">' + incidence.title 
								+ '</span><span class="legend-fecha">' + incidence.fecha +'</span><span class="legend-gestor">'+ incidence.gestor 
								+ '</span><span class="legend-estado">' + incidence.estado + '</span></a></li>');
		});
	}
});

$("#newincidencia").live("pageinit", function(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; 
	var yyyy = today.getFullYear();
	if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} today = mm+'/'+dd+'/'+yyyy;
	$("#Date").val(today);
});

$("#equipos").live("pageinit", function(){
	LoadEquipo();
});

$("#IncidenceDetail").live('pageinit', function(){
	divCloned = $("#coments").clone();
});

$("#IncidenceDetail").live('pageshow', function(){
	loadIncidencia();
});

$("#IncidenceDetail").live('pagehide', function(){
	$('#coments').replaceWith(divCloned);
});

/* -- FUNCIONES DATABASE*/
function loadIncidencia() {
	var parametres = { id: IncidenceDetailedID};
	var dataV = jQuery.param(parametres);
	
	$.ajax({
		type: 'GET',
		url: 'webservice/getIncidence.php',
		data: dataV,
		dataType: 'json',
		error: function(error) {alert('ERROR:  ' + error.message)}
	}).done(function(data) {
    			incidencias = data;
				$.each(incidencias, function(index, incidence) {
				$('#TIncidencia').val(incidence.title);
				$('#DIncidencia').val(incidence.descripcion);
				$('#TDate').val(incidence.fecha);
				
				var myselect = $("#StateIncidencia");				
				if(incidence.estado == "ABIERTA"){
					myselect[0].selectedIndex = 0;
				} else {
					myselect[0].selectedIndex = 1;
				}
				myselect.selectmenu("refresh");
				$('#id').val(incidence.id);
				IncidenceDetailedID =  incidence.id;
				LoadComentarios();
		});
	});
}

function SaveIncidencia(){
	var title = document.getElementById('TitleIncidencia').value;
	var description = document.getElementById('DescripcionIncidencia').value;
	var fecha = document.getElementById('Date').value;
	var gestor = "ADMIN";
	var estado = "ABIERTA";
	var user = '<?php echo $_SESSION["user"]; ?>';
	var params = { title: title, descript: description, fecha: fecha, user: user, gestor: gestor, estado: estado};
	var  dataT =  jQuery.param(params);
	
	$.ajax({
		type: 'POST',
		url: 'webservice/InsertIncidence.php',
		data: dataT,
		success: Correct,
		error: ErrorMessage
	});
	
}

function DelIncidencia(){
	var dataY =  { id: IncidenceDetailedID};
	var dataO = jQuery.param(dataY);
		$.ajax({
			type: 'POST',
			url: 'webservice/DelIncidence.php',
			data: dataO,
			error: function(error) {alert('ERROR:  ' + error.message)}
		}).done(function(data) {
					alert('inidencia Purgada');
	    			window.location = 'home.php#incidencias';
	    			location.reload();
			});
}

function UpdateIncidencia(id){
	
	var Selected =  document.getElementById('StateIncidencia').selectedIndex;
	var item  = document.getElementById("StateIncidencia").options;
	
	var title = document.getElementById('TIncidencia').value;
	var description = document.getElementById('DIncidencia').value;
	var fecha = document.getElementById('TDate').value;
	var estado = item[Selected].text
	var user = '<?php echo $_SESSION["user"]; ?>';
	var params = { id: IncidenceDetailedID,  title: title, descript: description, fecha: fecha, user: user, estado: estado };
	var  dataU =  jQuery.param(params);
	$.ajax({
		type: 'POST',
		url: 'webservice/UpdateIncidence.php',
		data: dataU,
		success: Updated,
		error: function(error) {alert('ERROR:  ' + error.message)}
	});
}

function UpdateEquipo(){
	
	var Nequipo = document.getElementById('NEquipo').value;
	var Mequipo = document.getElementById('Marca').value;
	var NSequipo = document.getElementById('CodigoS').value;
	var FCequipo =  document.getElementById('DateCompra').value;
	
	var pars =  {nombre: NSequipo, marca: Mequipo, nserie: NSequipo, fecha: FCequipo };
	var dataUE = jQuery.param(pars);
	
	$.ajax({
		type: 'POST',
		url: 'webservice/UpdateEquipo.php',
		data: dataUE,
		success: CorrectEquipo,
		error: function(error) {alert('ERROR:  ' + error.message)}
	});
	
}

function LoadEquipo(){
	$.ajax({
		type: 'POST',
		url: 'webservice/getEquipo.php',
		dataType: 'json',
		error: function(error) {alert('ERROR:  ' + error.message)}			
	}).done(function(data){
			equipos = data;
			$.each(equipos, function(index, equipo) {
				$('#NEquipo').val(equipo.nombre);
				$('#Marca').val(equipo.marca);
				$('#CodigoS').val(equipo.codS);
				$('#DateCompra').val(equipo.fechaC);
			});
	});
	
}

function LoadComentarios(){
	var dat = { id: IncidenceDetailedID };
	var dataLC = jQuery.param(dat);
	
	$.ajax({
		type: 'GET',
		url: 'webservice/getComentarios.php',
		data: dataLC,
		dataType: 'json',
		success: apenndi,
		error: function(error) {alert('ERROR:  ' + error.message)}
	});
	
	function apenndi(data){
		comentarios = data;
		$.each(comentarios, function(index, comentario) {
		$('#coments').append('<ul data-role="listview" data-inset="true" id="comentario" class="ui-listview ui-listview-inset ui-corner-all ui-shadow">'
															+ ' <li data-role="list-divider" role="heading" class="ui-li ui-li-divider ui-bar-c ui-first-child ui-last-child"> '  + comentario.idUser +  ' -- ' + comentario.fechaC + '</li>'
															+ ' <br><div id="textoComentario"> ' + comentario.Texto +' </div><br><br></ul>');
		});
	}
}

function InsertComment(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; 
	var yyyy = today.getFullYear();
	if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} today = mm+'/'+dd+'/'+yyyy;
	var fechaC = today;
	var texto = document.getElementById('NComment').value;
	var idIncidencia = IncidenceDetailedID;
	
	var pars = {idIncidencia: idIncidencia, texto: texto, fecha: fechaC};
	var dataIC = jQuery.param(pars);
	
	$.ajax({
		type: 'POST',
		url: 'webservice/InsertComment.php',
		data: dataIC,
		success: AppendC,
		error: function(error) {alert('ERROR:  ' + error.message)}
	});
	
	function AppendC(){
				$('#coments').append('<ul data-role="listview" data-inset="true"id="comentario" class="ui-listview ui-listview-inset ui-corner-all ui-shadow">'
															+ ' <li data-role="list-divider" role="heading" class="ui-li ui-li-divider ui-bar-c ui-first-child ui-last-child"> '  + user +  ' -- ' + fechaC + '</li>'
															+ ' <br><div id="textoComentario"> ' + texto +' </div><br><br></ul>');
				$('#NComment').val("");
	}
}

function loadProfile(){
			var $content  = '<li><label for="a">Antigua Contraseña</label><input type="password" id="a" class="ui-input-text ui-body-c">'
												+ '<label for="b">Nueva Contraseña</label><input type="password" id="b" class="ui-input-text ui-body-c">'
												+ '<label for="c">Repite Nueva Contraseña</label><input type="password" id="c" class="ui-input-text ui-body-c">'
												+ '</li>'
												+ '<a data-role="button" onclick="saveProfile()" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-theme="c" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-up-c">'
												+ '<span class="ui-btn-inner"><span class="ui-btn-text">GUARDAR</span></span></a>';
			$('#profileList').append($content).listview('refresh');
}

function saveProfile(){
	var Old = document.getElementById('a').value;
	var New = document.getElementById('b').value;
	var New1 = document.getElementById('c').value;
	if(New != New1){
		var $error = '<li style="color: #ff0000;">Las contraseñas no coinciden</li>';
		$('#profileList').append($error).listview('refresh');
		$('#a').val("");
		$('#b').val("");
		$('#c').val("");
	} else {
		checkPassword(Old, New);
		
		
	}
}

function checkPassword(password, New){
	$.ajax({
		type: 'GET',
		url:  'webservice/chekPass.php',
		dataType: 'json',
		error: function(error) {alert('ERROR:  ' + error.message)}
	}).done(function(data){
		pass =  data;
		$.each(pass, function (index, Udata){
			if (Udata.password == password){
					changeUserPasword(New);
			} else {
					alert('Las antigua no es la correcta');
			}
		});
	});
}

function changeUserPasword(NewP){
	var datos = {p: NewP};
	var dataNP = jQuery.param(datos);
	$.ajax({
		type: 'GET',
		data: dataNP,
		url: 'webservice/UpdateUserData.php',
		error: function(error) {alert('ERROR:  ' + error.message)}
	}).done(function(data){
		alert('Datos modificados');	
		if(NewP.length < 6){
			var $error = '<li style="color: #FDD017;">Recomendamos usar contraseñas de mas de 6 caracteres</li>';
			$('#profileList').append($error).listview('refresh');
		}
		$('#a').val("");
		$('#b').val("");
		$('#c').val("");
	});
	
}
/* -- FUNCIONES DE FINALIZACION --*/

function CorrectEquipo(){
	alert('Actualizados los datos del equipo');
	window.location = '#home';
}

function Correct(){
	alert('Insertado OK!');
	$("#TitleIncidencia").val("");
	$("#DescripcionIncidencia").val("");
	window.location = '/home.php#incidencias';
	location.reload();
}

function ErrorMessage(Error){
	alert('ERROR:  Updateando la incidencia');
}

function GotoDetail(id){
	IncidenceDetailedID =  id;
	window.location = '#IncidenceDetail';
}

function logout(){
	window.location = '/webservice/logout.php';
}

function Updated(){
	alert('Datos modificados satisfactoriamente');
	window.location = '/home.php#incidencias';
	location.reload();
}
